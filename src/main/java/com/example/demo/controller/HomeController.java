package com.example.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping; 
import org.springframework.web.bind.annotation.GetMapping; 
import org.springframework.web.bind.annotation.RestController;  
@RestController  
@RequestMapping("/api")
public class HomeController {
	
	@GetMapping("/values")  
	public String hello()   
	{  
	return "WELCOME TO PHEME SOFTWARE";  
	} 

}
